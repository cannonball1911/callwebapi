﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CallWebAPI
{
    public partial class Form1 : Form
    {
        HttpClient client;
        List<Control> inputFields;

        public Form1()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.BaseAddress = new Uri("http://localhost:3000/api/");
            inputFields = new List<Control>
            {
                TxbDauerEdit,
                TxbDauerNew,
                TxbIdEditDelete,
                TxbRezeptEdit,
                TxbRezeptNew
            };
        }

        #region Get
        private async void BtnGo_Click(object sender, EventArgs e)
        {
            string jsonAllRezepte = await GetRezeptJsonAsync();
            TxbJsonResult.Text = jsonAllRezepte;
            UpdateDgvRezepte(jsonAllRezepte);
        }
        #endregion

        #region Delete
        private async void BtnDelete_Click(object sender, EventArgs e)
        {
            string id = TxbIdEditDelete.Text;
            HttpResponseMessage response = await client.DeleteAsync($"rezepte/{id}");
            if (!response.IsSuccessStatusCode)
            {
                MessageBox.Show("Nix gefunden zum löschen :(");
                return;
            }
            string jsonResult = await response.Content.ReadAsStringAsync();
            RootObject delRezept = JsonConvert.DeserializeObject<RootObject>(jsonResult);
            string msg = $"{delRezept.data[0].rezepteID} : {delRezept.data[0].rezept}";
            MessageBox.Show(msg, "Delete");
            UpdateDgvRezepte(await GetRezeptJsonAsync());
            ClearInputFieldsAndSelectNone();
        }
        #endregion

        #region Create
        private async void BtnNew_Click(object sender, EventArgs e)
        {
            string rezept = TxbRezeptNew.Text;
            int.TryParse(TxbDauerNew.Text, out int zubereitungsdauer);

            StringContent content = PrepareContent(0, rezept, zubereitungsdauer);

            HttpResponseMessage response = await client.PostAsync("rezepte", content);
            if (!response.IsSuccessStatusCode)
            {
                MessageBox.Show("Error");
                return;
            }
            string jsonResult = await response.Content.ReadAsStringAsync();
            if (jsonResult.StartsWith("ERROR"))
            {
                MessageBox.Show("Error");
                return;
            }
            RootObject createdRezept = JsonConvert.DeserializeObject<RootObject>(jsonResult);

            string msg = $"ID: {createdRezept.data[0].rezepteID} - Rezept: {createdRezept.data[0].rezept} - Dauer: {createdRezept.data[0].zubereitungsdauer}";
            MessageBox.Show(msg, "Create");
            UpdateDgvRezepte(await GetRezeptJsonAsync());
            ClearInputFieldsAndSelectNone();
        }
        #endregion

        #region Edit
        private async void BtnEdit_Click(object sender, EventArgs e)
        {
            bool idParseResult = int.TryParse(TxbIdEditDelete.Text, out int id);
            string rezept = TxbRezeptEdit.Text;
            bool dauerParseResult = int.TryParse(TxbDauerEdit.Text, out int zubereitungsdauer);

            StringContent content = PrepareContent(id, rezept, zubereitungsdauer);

            HttpResponseMessage response = await client.PutAsync($"rezepte/{id}", content);
            if (!response.IsSuccessStatusCode)
            {
                MessageBox.Show("Error");
                return;
            }
            UpdateDgvRezepte(await GetRezeptJsonAsync());
            ClearInputFieldsAndSelectNone();
        }
        #endregion

        #region DGV_Events
        private void DgvRezepte_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int selectedRowIndex = e.RowIndex;
            DataGridView dgv = (DataGridView)sender;
            TxbIdEditDelete.Text = dgv.Rows[selectedRowIndex].Cells[0].Value.ToString();
            TxbRezeptEdit.Text = dgv.Rows[selectedRowIndex].Cells[1].Value.ToString();
            TxbDauerEdit.Text = dgv.Rows[selectedRowIndex].Cells[2].Value.ToString();
        }
        #endregion

        #region Tools
        private void ClearInputFieldsAndSelectNone()
        {
            foreach (Control control in inputFields)
            {
                control.Text = "";
            }
            DgvRezepte.ClearSelection();
        }

        /// <summary>
        /// Updates the DataGridView
        /// </summary>
        /// <param name="jsonRezepte"></param>
        private void UpdateDgvRezepte(string jsonRezepte)
        {
            RootObject rezepte = JsonConvert.DeserializeObject<RootObject>(jsonRezepte);
            DgvRezepte.DataSource = rezepte.data;
        }

        /// <summary>
        /// Gets json string of rezepte
        /// </summary>
        /// <returns>Task<string></returns>
        private async Task<string> GetRezeptJsonAsync()
        {
            Task<string> rezeptJson = client.GetStringAsync("rezepte");
            return await rezeptJson;
        }

        /// <summary>
        /// Prepare content for creating/editing a rezept
        /// </summary>
        /// <param name="id"></param>
        /// <param name="rezept"></param>
        /// <param name="zubereitungsdauer"></param>
        /// <returns>StringContent</returns>
        private StringContent PrepareContent(int id, string rezept, int zubereitungsdauer)
        {
            Rezept rz = new Rezept
            {
                rezepteID = id,
                rezept = rezept,
                zubereitungsdauer = zubereitungsdauer
            };
            string jsonString = JsonConvert.SerializeObject(rz);
            StringContent content = new StringContent(jsonString, UnicodeEncoding.UTF8, "application/json");
            return content;
        }
        #endregion
    }
}
