﻿using System.Collections.Generic;

namespace CallWebAPI
{
    public class Rezept
    {
        public int rezepteID { get; set; }
        public string rezept { get; set; }
        public int zubereitungsdauer { get; set; }
    }

    public class RootObject
    {
        public List<Rezept> data { get; set; }
    }
}
