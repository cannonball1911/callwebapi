﻿namespace CallWebAPI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnGo = new System.Windows.Forms.Button();
            this.TxbJsonResult = new System.Windows.Forms.TextBox();
            this.DgvRezepte = new System.Windows.Forms.DataGridView();
            this.TxbIdEditDelete = new System.Windows.Forms.TextBox();
            this.TxbDauerEdit = new System.Windows.Forms.TextBox();
            this.TxbRezeptNew = new System.Windows.Forms.TextBox();
            this.BtnEdit = new System.Windows.Forms.Button();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnNew = new System.Windows.Forms.Button();
            this.TxbDauerNew = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxbRezeptEdit = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DgvRezepte)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnGo
            // 
            this.BtnGo.Location = new System.Drawing.Point(12, 12);
            this.BtnGo.Name = "BtnGo";
            this.BtnGo.Size = new System.Drawing.Size(75, 23);
            this.BtnGo.TabIndex = 0;
            this.BtnGo.Text = "Go";
            this.BtnGo.UseVisualStyleBackColor = true;
            this.BtnGo.Click += new System.EventHandler(this.BtnGo_Click);
            // 
            // TxbJsonResult
            // 
            this.TxbJsonResult.Location = new System.Drawing.Point(12, 41);
            this.TxbJsonResult.Multiline = true;
            this.TxbJsonResult.Name = "TxbJsonResult";
            this.TxbJsonResult.Size = new System.Drawing.Size(241, 397);
            this.TxbJsonResult.TabIndex = 1;
            // 
            // DgvRezepte
            // 
            this.DgvRezepte.AllowUserToAddRows = false;
            this.DgvRezepte.AllowUserToDeleteRows = false;
            this.DgvRezepte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvRezepte.Location = new System.Drawing.Point(259, 41);
            this.DgvRezepte.Name = "DgvRezepte";
            this.DgvRezepte.Size = new System.Drawing.Size(392, 397);
            this.DgvRezepte.TabIndex = 2;
            this.DgvRezepte.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvRezepte_CellClick);
            // 
            // TxbIdEditDelete
            // 
            this.TxbIdEditDelete.Location = new System.Drawing.Point(657, 65);
            this.TxbIdEditDelete.Name = "TxbIdEditDelete";
            this.TxbIdEditDelete.ReadOnly = true;
            this.TxbIdEditDelete.Size = new System.Drawing.Size(100, 20);
            this.TxbIdEditDelete.TabIndex = 3;
            // 
            // TxbDauerEdit
            // 
            this.TxbDauerEdit.Location = new System.Drawing.Point(860, 65);
            this.TxbDauerEdit.Name = "TxbDauerEdit";
            this.TxbDauerEdit.Size = new System.Drawing.Size(96, 20);
            this.TxbDauerEdit.TabIndex = 4;
            // 
            // TxbRezeptNew
            // 
            this.TxbRezeptNew.Location = new System.Drawing.Point(763, 165);
            this.TxbRezeptNew.Name = "TxbRezeptNew";
            this.TxbRezeptNew.Size = new System.Drawing.Size(96, 20);
            this.TxbRezeptNew.TabIndex = 6;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Location = new System.Drawing.Point(763, 91);
            this.BtnEdit.Name = "BtnEdit";
            this.BtnEdit.Size = new System.Drawing.Size(75, 23);
            this.BtnEdit.TabIndex = 7;
            this.BtnEdit.Text = "Edit";
            this.BtnEdit.UseVisualStyleBackColor = true;
            this.BtnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Location = new System.Drawing.Point(881, 91);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(75, 23);
            this.BtnDelete.TabIndex = 8;
            this.BtnDelete.Text = "Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(657, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(857, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Zubereitungsdauer";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(762, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Rezept";
            // 
            // BtnNew
            // 
            this.BtnNew.Location = new System.Drawing.Point(823, 191);
            this.BtnNew.Name = "BtnNew";
            this.BtnNew.Size = new System.Drawing.Size(75, 23);
            this.BtnNew.TabIndex = 13;
            this.BtnNew.Text = "New Record";
            this.BtnNew.UseVisualStyleBackColor = true;
            this.BtnNew.Click += new System.EventHandler(this.BtnNew_Click);
            // 
            // TxbDauerNew
            // 
            this.TxbDauerNew.Location = new System.Drawing.Point(860, 165);
            this.TxbDauerNew.Name = "TxbDauerNew";
            this.TxbDauerNew.Size = new System.Drawing.Size(96, 20);
            this.TxbDauerNew.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(857, 149);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Zubereitungsdauer";
            // 
            // TxbRezeptEdit
            // 
            this.TxbRezeptEdit.Location = new System.Drawing.Point(763, 65);
            this.TxbRezeptEdit.Name = "TxbRezeptEdit";
            this.TxbRezeptEdit.Size = new System.Drawing.Size(96, 20);
            this.TxbRezeptEdit.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(763, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Rezept";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(965, 457);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TxbRezeptEdit);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxbDauerNew);
            this.Controls.Add(this.BtnNew);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnDelete);
            this.Controls.Add(this.BtnEdit);
            this.Controls.Add(this.TxbRezeptNew);
            this.Controls.Add(this.TxbDauerEdit);
            this.Controls.Add(this.TxbIdEditDelete);
            this.Controls.Add(this.DgvRezepte);
            this.Controls.Add(this.TxbJsonResult);
            this.Controls.Add(this.BtnGo);
            this.Name = "Form1";
            this.Text = "CallWebAPI";
            ((System.ComponentModel.ISupportInitialize)(this.DgvRezepte)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnGo;
        private System.Windows.Forms.TextBox TxbJsonResult;
        private System.Windows.Forms.DataGridView DgvRezepte;
        private System.Windows.Forms.TextBox TxbIdEditDelete;
        private System.Windows.Forms.TextBox TxbDauerEdit;
        private System.Windows.Forms.TextBox TxbRezeptNew;
        private System.Windows.Forms.Button BtnEdit;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button BtnNew;
        private System.Windows.Forms.TextBox TxbDauerNew;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxbRezeptEdit;
        private System.Windows.Forms.Label label3;
    }
}

